#include "c_vector.h"
#include <malloc.h>
#include <string.h>

struct vector
{
    void**   data;
    size_t   count;
};

c_vector vector_create()
{
    return calloc(0, sizeof(struct vector));
}

void vector_destroy(c_vector vector)
{
    struct vector *vec = vector;
    size_t i;

    if (vector) {
        for (i = 0; i < vec->count; i++) {
            free(vec->data[i]);
        }
        free(vec->data);
        free(vector);
    }
}

void vector_push_back(c_vector vector, void *data, size_t length)
{
    struct vector *vec = vector;

    vec->count++;    
    vec->data = vec->data == NULL ?
        (void**)malloc(vec->count * sizeof(size_t)) :
        (void**)realloc(vec->data, vec->count * sizeof(size_t)) ;
    vec->data[vec->count - 1] = malloc(length);
    memcpy(vec->data[vec->count - 1], data, length);
}

void vector_pop_back(c_vector vector)
{
    struct vector *vec = vector;

    if (vec->count) {
        vec->count--;
        free(vec->data[vec->count]);
        vec->data = (void**)realloc(vec->data, vec->count * sizeof(size_t));
    }
}

void vector_erase(c_vector vector, size_t index)
{
    struct vector *vec = vector;
    size_t        i;

    if (vec->count > index) {
        vec->count--;
        free(vec->data[index]);
        for (i = 0; i < vec->count - index; i++) {
            vec->data[i + index] = vec->data[i + index + 1];
        }
        vec->data = (void**)realloc(vec->data, vec->count * sizeof(size_t));
    }
}

void *vector_data(c_vector vector, size_t index)
{
    struct vector *vec = vector;
    return vec->data[index];
}

size_t vector_count(c_vector vector)
{
    struct vector *vec = vector;
    return vec->count;
}

