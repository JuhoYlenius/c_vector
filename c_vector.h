#ifndef C_VECTOR_H
#define C_VECTOR_H

#include <inttypes.h>

typedef void *c_vector;
typedef __SIZE_TYPE__ size_t;

c_vector vector_create();
void vector_destroy(c_vector vector);
void vector_push_back(c_vector vector, void *data, size_t length);
void vector_pop_back(c_vector vector);
void vector_erase(c_vector vector, size_t index);
void *vector_data(c_vector vector, size_t index);
uint64_t vector_count(c_vector vector);

#endif

