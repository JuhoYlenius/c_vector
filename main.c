#include "c_vector.h"
#include <stdio.h>

int main(void)
{
    c_vector vec = vector_create();
    unsigned i;

    for (i = 0; i < 10; i++) {
        vector_push_back(vec, &i, sizeof(unsigned));
    }
    for (i = 0; i < 10; i++) {
        printf("%d\n", *(unsigned*)vector_data(vec, i));
    }
    vector_destroy(vec);
}

